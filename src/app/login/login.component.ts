import { AuthenticationService } from '../core/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private loadingController: LoadingController,
              private authenticationService: AuthenticationService) {
    this.createForm();
  }

  ngOnInit() {}

  login() {
    const loading = this.loadingController.create();
    //await loading.present();
    this.authenticationService.login(this.loginForm.value)
      .pipe(finalize(() => {
        this.loginForm.markAsPristine();
        //await loading.dismiss();
      }))
      .subscribe(credentials => {
        this.route.queryParams.subscribe(
          params => this.router.navigate([ params.redirect || '/'], { replaceUrl: true })
        );
      }, error => {
        console.log(error);
      });
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true
    });
  }

}
